import { FETCH_APPLICATION, NEW_APPLICATION } from './types';


export const createApplication = (projectData) => dispatch => {
  console.log('action called');
  fetch('http://192.168.22.39:4000/api/applications', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(projectData)
  })
    .then(res => res.json())
    .then(obj => dispatch({
      type: NEW_APPLICATION,
      payload: obj
    }));
  console.log('Successfully added....')
  console.log(projectData);
};

export const fetchApplication = () => dispatch => {
  fetch('http://192.168.22.39:4000/api/applications')
    .then(res => res.json())
    .then(applications =>
      dispatch({
        type: FETCH_APPLICATION,
        payload: applications
      })
    );
}
