import { FETCH_TOOL, NEW_TOOL } from './types';

export const createTool = (projectData) => dispatch => {
  console.log('action called');
  fetch('http://192.168.22.39:4000/api/tools', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(projectData)
  })
    .then(res => res.json())
    .then(obj => dispatch({
      type: NEW_TOOL,
      payload: obj
    })).catch(err => console.log(err));
  console.log('Successfully added....')
  console.log(projectData);
};

export const fetchTool = () => dispatch => {
  fetch('http://192.168.22.39:4000/api/tools')
    .then(res => res.json())
    .then(projects =>
      dispatch({
        type: FETCH_TOOL,
        payload: projects
      })
    );
}
