export const LOGIN = "LOGIN";
export const FETCH_POSTS = "FETCH_POSTS";
export const GET_CONVERSION_RATE = "GET_CONVERSION_RATE";
export const GET_USD_RATE = "GET_USD_RATE";
export const TOGGLE_PANEL = "TOGGLE_PANEL";
export const GET_EUR_RATE = "GET_EUR_RATE";
export const GET_MODEL = "GET_MODEL";
export const VIN_VALIDATE = "VIN_VALIDATE";
export const SAVE_CONTRACT = "SAVE_CONTRACT";
export const DEPLOY_CONTRACT = "DEPLOY_CONTRACT";
//admin-management
export const NEW_APPLICATION = 'NEW_APPLICATION';
export const FETCH_APPLICATION = 'FETCH_APPLICATION';
export const NEW_TOOL = 'NEW_TOOL';
export const FETCH_TOOL = 'FETCH_TOOL';
export const NEW_PROJECT = 'NEW_PROJECT';
export const FETCH_PROJECT = 'FETCH_PROJECT';
