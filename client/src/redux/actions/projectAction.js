import { FETCH_PROJECT, NEW_PROJECT } from './types';
//import Axios from 'axios';

export const createProject = projectData => dispatch => {
  console.log('action called');
  fetch('http://192.168.22.39:4000/api/projects', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(projectData)
  })
    .then(res => res.json())
    .then(obj => dispatch({
      type: NEW_PROJECT,
      payload: obj
    }));
};

export const fetchProject = () => dispatch => {
  fetch('http://192.168.22.39:4000/api/projects')
    .then(res => res.json())
    .then(projects =>
      dispatch({
        type: FETCH_PROJECT,
        payload: projects
      })
    );
}
