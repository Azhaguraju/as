import {LOGIN} from './types';

export const loginAction=(data) => dispatch=> {
    // This where the login functionality is included

    let loginData = {};

    if(data.userName === 'adheep@ymail.com' && data.password === 'welcome1') {
        loginData.userName = data.userName;
        loginData.password = data.password;
        loginData.isLoggedIn = true;
        loginData.redirect = true;
        loginData.firstName = 'Adheep';
        loginData.lastName = 'Mohamed';
        loginData.role = 'customer';
        loginData.avatar = '/static/media/avatar.28158ce2.png'
    } else {
        loginData.isLoggedIn = false;
        loginData.redirect = false;
    }
    dispatch({
        type: LOGIN,
        data: loginData
    });
}