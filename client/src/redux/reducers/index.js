import { combineReducers } from 'redux';
import loginReducer from './loginReducer.js';
import applicationReducer from "../reducers/applicationReducer";
import toolReducer from "../reducers/toolReducer";
import projectReducer from "../reducers/projectReducer";

export default combineReducers({
  loginData: loginReducer,
  application: applicationReducer,
  tool: toolReducer,
  project: projectReducer,
});
