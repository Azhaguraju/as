import {LOGIN} from '../actions/types';

const initialState = {
    userName: '',
    password: '',
    isLoggedIn: false,
    redirect: false
}

export default function(state = initialState, action) {
    switch(action.type) {
        case LOGIN:
            return {
                ...state,
                ...action.data
            }
        default:
                return state;
    }
}