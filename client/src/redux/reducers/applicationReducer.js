import { FETCH_APPLICATION, NEW_APPLICATION } from '../actions/types';

const initialState = {
  items: [],
  item: {}
};


export default function (state = initialState, action) {
  switch (action.type) {
    case NEW_APPLICATION:
      console.log("inside new app reducer")
      return {
        ...state,
        item: action.payload
      };
    case FETCH_APPLICATION:
      return {

        ...state,
        items: action.payload
      }
    default:
      return state;
  }
}

