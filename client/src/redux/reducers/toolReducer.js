import { FETCH_TOOL, NEW_TOOL } from '../actions/types';

const initialState = {
    items: [],
    item: {}
};


export default function (state = initialState, action) {
    switch (action.type) {
        case NEW_TOOL:
            return {
                ...state,
                item: action.payload
            };
        case FETCH_TOOL:
            return {
                // ...state,
                items: action.payload
            }
        default:
            return state;
    }
}