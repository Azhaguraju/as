
import React from 'react';
// import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createTool } from '../redux/actions/toolAction';
// import './index.css';
import { Form, Input, Button, Switch, Select } from 'antd';
const { Option } = Select;

class AddTool extends React.Component {
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.props.createTool(values);
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (

            <Form {...formItemLayout} onSubmit={this.handleSubmit}>

                <Form.Item label="Name">
                    {getFieldDecorator('tool_name', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter name',
                            },
                            {
                                max: 20,
                                message: "name should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "name should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Version">
                    {getFieldDecorator('tool_version', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter version',
                            },
                            {
                                max: 20,
                                message: "version should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "version should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Technology">
                    {getFieldDecorator('technology', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter technology',
                            },
                            {
                                max: 20,
                                message: "technology should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "technology should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Window Objects">
                    {getFieldDecorator('window_objects', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter window objects',
                            },
                            {
                                max: 20,
                                message: "window objects should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "window objects should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Screen Objects">
                    {getFieldDecorator('screen_objects', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter screen objects',
                            },
                            {
                                max: 20,
                                message: "screen objects should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "screen objects should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Status">
                    {getFieldDecorator('status', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter status',
                            },
                            {
                                max: 20,
                                message: "status should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "status should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Field Objects">
                    {getFieldDecorator('field_objects', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter field objects',
                            },
                            {
                                max: 20,
                                message: "field objects should be accept maximum 20 characters"
                            },
                            {
                                min: 4,
                                message: "field objects should be minimum 4 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Visibility">
                    {getFieldDecorator('visibility', {
                        initialValue: 'public',
                        rules: [
                            {
                                required: true,
                                message: 'Please input your Role!'
                            }
                        ],
                    })(<Select style={{ width: 200 }}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                    </Select>)}
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Create Tool
          </Button>
                </Form.Item>

            </Form>
        );
    }
}

AddTool.propTypes = {
    createTool: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    item: state.tool.item
});

const WrappedCreatetool = Form.create({ name: 'register' })(AddTool);
export default connect(mapStateToProps, { createTool })(WrappedCreatetool);

