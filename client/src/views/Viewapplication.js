import React from 'react';
import 'antd/dist/antd.css';
import { Table } from 'antd';
import { Input, Button, Icon, Form } from 'antd';
import Highlighter from 'react-highlight-words';
import { connect } from 'react-redux';
import { fetchApplication } from '../redux/actions/applicationAction';
import PropTypes from 'prop-types';

export class Viewapplication extends React.Component {
  componentWillMount() {
    console.log("fetchApplication inside");
    this.props.fetchApplication();
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
      </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
      </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const data = [];
    {
      this.props.items.map(object =>
        data.push({
          application_id: object.application_id,
          application_name: object.application_name,
          application_version: object.application_version,
          api_technology: object.api_technology,
          techstack: object.techstack,
          environment: object.environment,
          developed_by: object.developed_by,
          implemented_by: object.implemented_by,
          visibility: object.visibility,
          status: object.status,
          created_by: object.created_by,
          created_on: object.created_on,
          updated_by: object.updated_by,
          // updated_on: object.updated_on,
        }),
      );
    }

    const columns = [
      {
        title: 'Application Id',
        dataIndex: 'application_id',
        key: 'application_id',
        ...this.getColumnSearchProps('application_id'),
      },
      {
        title: 'Application Name',
        dataIndex: 'application_name',
        key: 'application_name',
        ...this.getColumnSearchProps('application_name'),
      },
      {
        title: 'Application Version',
        dataIndex: 'application_version',
        key: 'application_version',
        ...this.getColumnSearchProps('application_version'),
      },
      {
        title: 'Api Technology',
        dataIndex: 'api_technology',
        key: 'api_technology',
        ...this.getColumnSearchProps('api_technology'),
      },
      {
        title: 'Techstack',
        dataIndex: 'techstack',
        key: 'techstack',
        ...this.getColumnSearchProps('techstack'),
      },
      {
        title: 'Environment',
        dataIndex: 'environment',
        key: 'environment',
        ...this.getColumnSearchProps('environment'),
      },
      {
        title: 'Developed By',
        dataIndex: 'developed_by',
        key: 'developed_by',
        ...this.getColumnSearchProps('developed_by'),
      },
      {
        title: 'Implemented By',
        dataIndex: 'implemented_by',
        key: 'implemented_by',
        ...this.getColumnSearchProps('implemented_by'),
      },
      {
        title: 'Visibility',
        dataIndex: 'visibility',
        key: 'visibility',
        ...this.getColumnSearchProps('visibility'),
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        ...this.getColumnSearchProps('status'),
      },
      {
        title: 'Created By',
        dataIndex: 'created_by',
        key: 'created_by',
        ...this.getColumnSearchProps('created_by'),
      },
      {
        title: 'Created On',
        dataIndex: 'created_on',
        key: 'created_on',
        ...this.getColumnSearchProps('created_on'),
      },
      {
        title: 'Updated By',
        dataIndex: 'updated_by',
        key: 'updated_by',
        ...this.getColumnSearchProps('updated_by'),
      },
      // {
      //   title: 'Updated On',
      //   dataIndex: 'updated_on',
      //   key: 'updated_on',
      //   ...this.getColumnSearchProps('updated_on'),
      // }

    ];

    return (
      <div>
        <Table dataSource={data} columns={columns} pagination={{ pageSize: 28 }} rowKey='_id' />;
 <div style={{ 'color': 'red' }}></div>
      </div>
    )
  }
}

Viewapplication.propTypes = {
  fetchApplication: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired

}

const mapStateToProps = state => ({
  items: state.application.items
});

const ViewapplicationTable = Form.create({ name: "index" })(Viewapplication);
export default connect(mapStateToProps, { fetchApplication })(ViewapplicationTable);
