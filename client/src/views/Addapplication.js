import React from 'react';
// import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createApplication } from '../redux/actions/applicationAction';
// import './index.css';
import { Form, Input, Button, Switch, Select } from 'antd';
const { TextArea } = Input;
const { Option } = Select;


class Addapplication extends React.Component {

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.createApplication(values);
        console.log(values.visibility);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (

      <Form {...formItemLayout} onSubmit={this.handleSubmit}>

        <Form.Item label="Name">
          {getFieldDecorator('application_name', {
            rules: [
              {
                required: true,
                message: 'Please enter name',
              },
              {
                max: 20,
                message: "name should be accept maximum 10 characters"
              },
              {
                min: 4,
                message: "name should be minimum 3 characters"
              }
            ],
          })(<Input placeholder="Please enter name" />)}
        </Form.Item>

        <Form.Item label="Version">
          {getFieldDecorator('application_version', {
            rules: [
              {
                required: true,
                message: 'Please enter version',
              },
              {
                max: 20,
                message: "version should be accept maximum 10 characters"
              },
              {
                min: 2,
                message: "version should be minimum 3 characters"
              }
            ],
          })(<Input placeholder="Please enter name" />)}
        </Form.Item>

        <Form.Item label="US/APS Technology">
          {getFieldDecorator('api_technology', {
            rules: [
              {
                required: true,
                message: 'Please enter US/APS technology',
              },
              {
                max: 20,
                message: "US/APS technology should be accept maximum 10 characters"
              },
              {
                min: 4,
                message: "US/APS technology should be minimum 3 characters"
              }
            ],
          })(<Input placeholder="Please enter US/APS Technology" />)}
        </Form.Item>

        <Form.Item label="Tech Stack">
          {getFieldDecorator('techstack', {
            rules: [
              {
                required: true,
                message: 'Please enter techstock',
              },
              {
                max: 20,
                message: "techstack should be accept maximum 1000 characters"
              },
              {
                min: 4,
                message: "techstack should be minimum 3 characters"
              }
            ],
          })(<TextArea
            placeholder="Please enter tech stock"
            autosize={{ minRows: 3, maxRows: 3 }}
          />)}
        </Form.Item>

        <Form.Item label="Environment">
          {getFieldDecorator('environment', {
            rules: [
              {
                required: true,
                message: 'Please enter environment',
              },
              {
                max: 20,
                message: "environment should be accept maximum 10 characters"
              },
              {
                min: 4,
                message: "environment should be minimum 3 characters"
              }
            ],
          })(<Input placeholder="Please enter environment" />)}
        </Form.Item>

        <Form.Item label="Developed By">
          {getFieldDecorator('developed_by', {
            rules: [
              {
                required: true,
                message: 'Please enter developed by',
              },
              {
                max: 20,
                message: "developed by should be accept maximum 10 characters"
              },
              {
                min: 4,
                message: "developed by should be minimum 3 characters"
              }
            ],
          })(<Input placeholder="Please enter developed by" />)}
        </Form.Item>

        <Form.Item label="Implemented By">
          {getFieldDecorator('implemented_by', {
            rules: [
              {
                required: true,
                message: 'Please enter implemented by',
              },
              {
                max: 20,
                message: "implemented by should be accept maximum 10 characters"
              },
              {
                min: 4,
                message: "implemented by should be minimum 3 characters"
              }
            ],
          })(<Input placeholder="Please enter implemented by" />)}
        </Form.Item>

        <Form.Item label="Visibility">
          {getFieldDecorator('visibility', {
            initialValue: 'public',
            rules: [
              {
                required: true,
                message: 'Please input your Role!'
              }
            ],
          })(<Select style={{ width: 200 }}>
            <Option value="public">Public</Option>
            <Option value="private">Private</Option>
          </Select>)}
        </Form.Item>


        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Create Application
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

Addapplication.propTypes = {
  createApplication: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  item: state.application.item
});

const WrappedCreateapplication = Form.create({ name: 'register' })(Addapplication);

export default connect(mapStateToProps, { createApplication })(WrappedCreateapplication);

