import React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Select } from 'antd';
import { createProject } from '../redux/actions/projectAction';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import DynamicFields from "./DynamicFields";
const { TextArea } = Input;
const { Option } = Select;

class Createproject extends React.Component {


    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log(JSON.stringify(values))
                console.log('Received values of form: ', values);
                this.props.createProject(values);
            }
        });
    };

    render() {

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };
        const { form } = this.props;

        return (

            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item label="Name">
                    {getFieldDecorator('project_name', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter name',
                            },
                            {
                                max: 20,
                                message: "name should be accept maximum 10 characters"
                            },
                            {
                                min: 4,
                                message: "name should be minimum 3 characters"
                            }
                        ],
                    })(<Input placeholder="Please enter name" />)}
                </Form.Item>

                <Form.Item label="Project Description">
                    {getFieldDecorator('Project_Desc', {
                        rules: [
                            {
                                required: true,
                                message: 'Please enter techstock',
                            },
                            {
                                max: 20,
                                message: "techstock should be accept maximum 1000 characters"
                            },
                            {
                                min: 4,
                                message: "techstock should be minimum 3 characters"
                            }
                        ],
                    })(<TextArea
                        placeholder="Please enter tech stock"
                        autosize={{ minRows: 3, maxRows: 3 }}
                    />)}
                </Form.Item>

                <Form.Item label="Visibility">
                    {getFieldDecorator('visibility', {
                        initialValue: 'public',
                        rules: [
                            {
                                required: true,
                                message: 'Please input your Role!'
                            }
                        ],
                    })(<Select style={{ width: 200 }}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                    </Select>)}
                </Form.Item>
                <Form.Item label="Select User & Role">
                    <DynamicFields
                        {...form}
                        name="user"
                        fields={[
                            {
                                name: "username",
                                field: () => (
                                    <Select style={{ width: "100%" }} placeholder="Select a user">
                                        <Option value="raja">raja</Option>
                                        <Option value="kamatchi">kamatchi</Option>
                                        <Option value="vindhiya">vindhiya</Option>
                                    </Select>
                                )
                            },
                            {
                                name: "userrole",
                                field: () => (
                                    <Select style={{ width: "100%" }} placeholder="Select role">
                                        <Option value="developer">developer</Option>
                                        <Option value="admin">admin</Option>
                                        <Option value="guest">guest</Option>
                                    </Select>
                                )
                            }
                        ]}
                    />
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Create Project
          </Button>
                </Form.Item>
            </Form>
        );
    }
}
Createproject.propTypes = {
    createProject: PropTypes.func.isRequired,
    item: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    item: state.project.item

});

const WrappedCreateproject = Form.create({ name: 'register' })(Createproject);

export default connect(mapStateToProps, { createProject })(WrappedCreateproject);
