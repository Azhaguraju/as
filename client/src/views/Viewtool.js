import React from 'react';
import 'antd/dist/antd.css';
import { Table } from 'antd';
import { Input, Button, Icon, Form } from 'antd';
import Highlighter from 'react-highlight-words';
import { connect } from 'react-redux';
import { fetchTool } from '../redux/actions/toolAction';
import PropTypes from 'prop-types';
export class Viewtool extends React.Component {

    componentWillMount() {
        console.log("fetchTool inside");
        this.props.fetchTool();
    }

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
      </Button>
                <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
      </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[this.props.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    });

    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: "" });
    };
    render() {
        const data = [];
        {
            this.props.items.map(object =>
                data.push({
                    tool_id: object.tool_id,
                    tool_name: object.tool_name,
                    tool_version: object.tool_version,
                    technology: object.technology,
                    window_objects: object.window_objects,
                    screen_objects: object.screen_objects,
                    field_objects: object.field_objects,
                    visibility: object.visibility,
                    status: object.status,
                    created_by: object.created_by,
                    created_on: object.created_on,
                    updated_by: object.updated_by,
                    // updated_on: object.updated_on,
                }),
            );
        }
        const columns = [
            {
                title: 'Tool_id',
                dataIndex: 'tool_id',
                key: 'tool_id',
                ...this.getColumnSearchProps('tool_id'),
            },
            {
                title: 'Tool_name',
                dataIndex: 'tool_name',
                key: 'tool_name',
                ...this.getColumnSearchProps('tool_name'),
            },
            {
                title: 'Tool_version',
                dataIndex: 'tool_version',
                key: 'tool_version',
                ...this.getColumnSearchProps('tool_version'),
            },
            {
                title: 'Technology',
                dataIndex: 'technology',
                key: 'technology',
                ...this.getColumnSearchProps('technology'),
            },
            {
                title: 'Window Objects',
                dataIndex: 'window_objects',
                key: 'window_objects',
                ...this.getColumnSearchProps('window_objects'),
            },
            {
                title: 'Screen Objects',
                dataIndex: 'screen_objects',
                key: 'screen_objects',
                ...this.getColumnSearchProps('screen_objects'),
            },
            {
                title: 'Field Objects',
                dataIndex: 'field_objects',
                key: 'field_objects',
                ...this.getColumnSearchProps('field_objects'),
            },
            {
                title: 'Visibility',
                dataIndex: 'visibility',
                key: 'visibility',
                ...this.getColumnSearchProps('visibility'),
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
                ...this.getColumnSearchProps('status'),
            },
            {
                title: 'Created By',
                dataIndex: 'created_by',
                key: 'created_by',
                ...this.getColumnSearchProps('created_by'),
            },
            {
                title: 'Created On',
                dataIndex: 'created_on',
                key: 'created_on',
                ...this.getColumnSearchProps('created_on'),
            },
            {
                title: 'Updated By',
                dataIndex: 'updated_by',
                key: 'updated_by',
                ...this.getColumnSearchProps('updated_by'),
            },
            /* {
                 title: 'Updated On',
                 dataIndex: 'updated_on',
                 key: 'updated_on',
                 ...this.getColumnSearchProps('updated_on'),
               }*/

        ];

        return (
            <div>
                <Table dataSource={data} columns={columns} pagination={{ pageSize: 28 }} rowKey='_id' />;
 <div style={{ 'color': 'red' }}></div>
            </div>
        )
    }
}

Viewtool.propTypes = {
    fetchTool: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired

}

const mapStateToProps = state => ({
    items: state.tool.items
});

const ViewtoolTable = Form.create({ name: "index" })(Viewtool);
export default connect(mapStateToProps, { fetchTool })(ViewtoolTable);
