import React from 'react';
import 'antd/dist/antd.css';
import { Table } from 'antd';
import { Input, Button, Icon, Form } from 'antd';
import Highlighter from 'react-highlight-words';
import { connect } from 'react-redux';
import { fetchProject } from '../redux/actions/projectAction';
import PropTypes from 'prop-types';
export class Viewproject extends React.Component {
  componentWillMount() {
    console.log("fetchProject inside");
    this.props.fetchProject();
  }
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
      </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
      </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  render() {


    const data1 = [];
    {
      this.props.items.map(object =>
        data1.push({
          // project_id: object.project_id,
          project_name: object.project_name,
          // Project_Desc: object.Project_Desc,
          visibility: object.visibility,
          status: object.status,
          // user: object.user,
          //  user_id: object.user_id,
          //  user_name: object.user_name,
          //  role: object.role,
          project_admin: object.project_admin,
          created_by: object.created_by,
          created_on: object.created_on,
          //  updated_by: object.updated_by,
          //  updated_on: object.updated_on,
        }),
      );
    }

    console.log(1, this.props.items)
    const columns = [
      {
        title: 'Project Name',
        dataIndex: 'project_name',
        key: 'project_name',
        ...this.getColumnSearchProps('project_name'),
      },
      {
        title: 'Visibility',
        dataIndex: 'visibility',
        key: 'visibility',
        ...this.getColumnSearchProps('visibility'),
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        ...this.getColumnSearchProps('status'),
      },
      //  {
      //    title: 'User Details',
      //    dataIndex: 'user',
      //    key: 'user',
      //  ...this.getColumnSearchProps('user'),
      //  },
      {
        title: 'Project Admin',
        dataIndex: 'project_admin',
        key: 'project_admin',
        ...this.getColumnSearchProps('project_admin'),
      },
      {
        title: 'Created By',
        dataIndex: 'created_by',
        key: 'created_by',
        ...this.getColumnSearchProps('created_by'),
      },
      {
        title: 'Created On',
        dataIndex: 'created_on',
        key: 'created_on',
        ...this.getColumnSearchProps('created_on'),
      },
      /*  {
          title: 'Updated By',
          dataIndex: 'updated_by',
          key: 'updated_by',
          ...this.getColumnSearchProps('updated_by'),
        },
        {
          title: 'Updated On',
          dataIndex: 'updated_on',
          key: 'updated_on',
          ...this.getColumnSearchProps('updated_on'),
        }*/

    ];
    return (
      <div>
        <Table dataSource={data1} columns={columns} pagination={{ pageSize: 28 }} rowKey='_id' />;
 <div style={{ 'color': 'red' }}></div>
      </div>
    )
  }
}
Viewproject.propTypes = {
  fetchProject: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired
}
const mapStateToProps = state => ({
  items: state.project.items
});

const ViewprojectTable = Form.create({ name: "index" })(Viewproject);
export default connect(mapStateToProps, { fetchProject })(ViewprojectTable);
