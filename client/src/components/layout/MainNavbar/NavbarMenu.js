import React, { Component } from "react";
import { Menu, Icon } from "antd";

const { SubMenu } = Menu;

class NavbarMenu extends Component {

  render() {
    return (
      <div style={{ width: "75%" }}>
        <Menu
          mode="horizontal"
          defaultSelectedKeys={['0']}
          className="navHeaderMenu"
        >
          <SubMenu
            title={< span className="submenu-title-wrapper" > <Icon type="setting" />Application </span>}>
            <Menu.ItemGroup>
              <Menu.Item key="setting:2">
                <a href="/viewapplication">View Application</a>
              </Menu.Item>
              <Menu.Item key="setting:1">
                <a href="/addapplication">Add Application</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
          <SubMenu
            title={< span className="submenu-title-wrapper" > <Icon type="setting" />Project </span>}>
            <Menu.ItemGroup>
              <Menu.Item key="setting:1">
                <a href="/viewproject">View Project</a>
              </Menu.Item>
              <Menu.Item key="setting:2">
                <a href="/addproject">Add Project</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
          <SubMenu
            title={<span className="submenu-title-wrapper" > <Icon type="setting" />Tools </span>}>
            <Menu.ItemGroup>
              <Menu.Item key="setting:1">
                <a href="/viewtool">View Tool</a>
              </Menu.Item>
              <Menu.Item key="setting:2">
                <a href="/addtool">AddTools</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
        </Menu>

      </div>
    )
  }
}

export default NavbarMenu;