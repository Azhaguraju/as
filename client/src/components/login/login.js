import React, {Component} from 'react'
import "./login.css"
import "./all.css"
import "./bootstrap.min.css"
import {Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import {loginAction} from '../../redux/actions/loginAction'
import history from '../../history';
import message from '../messages/message'
export class login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            firstName: '',
            lastName: '',
            role: '',
            password: '',
            redirect: false,
            isLoggedIn: false,
            loginErrorMsg: ''

        }
        this.onChange = this
            .onChange
            .bind(this);

        this.onSubmit = this
            .onSubmit
            .bind(this);
    }

    onSubmit(e) {
        this.props.loginAction(this.state);
        e.preventDefault();
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    componentWillMount() {}

    componentWillReceiveProps(nextProps) {
        if (nextProps.loginData.redirect === true) {
            history.push("/");
        }
        this.setState(nextProps.loginData);
        if(this.state.isLoggedIn) {
            this.setState({
                loginErrorMsg: ''
            })
        } else {
            this.setState({
                loginErrorMsg: message.loginErrorMessage
            })
        }
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            console.log('redirecting..')
            return <Redirect to='/home'/>
        }
    }

    render() {
        return (
            <div name="container">
                {this.renderRedirect()}
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-4 mx-auto">
                        <div className="card card-signin my-5">
                            <div className="card-body">
                                <h5 className="card-title text-center">Sign In</h5>
                                <form className="form-signin" onSubmit={this.onSubmit}>
                                    <div className="form-label-group">
                                        <input
                                            type="email"
                                            id="userName"
                                            name="userName"
                                            className="form-control"
                                            placeholder="Email address"
                                            required
                                            onChange={this.onChange}/>
                                        <label>User Name</label>
                                    </div>

                                    <div className="form-label-group">
                                        <input
                                            type="password"
                                            id="password"
                                            name="password"
                                            className="form-control"
                                            placeholder="Password"
                                            onChange={this.onChange}
                                            required></input>
                                        <label>Password</label>
                                    </div>
                                    <div>
                                        <label className="loginErrorMessage">{this.state.loginErrorMsg}</label>
                                    </div>
                                    <div className="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                                        <label className="custom-control-label">Remember password!</label>
                                    </div>
                                    <button
                                        className="btn btn-lg btn-primary btn-block text-uppercase"
                                        type="submit">Sign in</button>
                                    <hr className="my-4"></hr>
                                    <button
                                        className="btn btn-lg btn-google btn-block text-uppercase disabled"
                                        type="submit"
                                        disabled>
                                        <i className="fab fa-google mr-2"></i>
                                        Sign in with Google</button>
                                    <button
                                        className="btn btn-lg btn-facebook btn-block text-uppercase disabled"
                                        type="submit"
                                        disabled>
                                        <i className="fab fa-facebook-f mr-2"></i>
                                        Sign in with Facebook</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({loginData: state.loginData});

export default connect(mapStateToProps, {loginAction})(login);
