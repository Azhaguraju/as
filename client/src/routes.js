import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, LoginLayout, LeftNavLayout } from "./layouts";

// Route Views
import Dashboard from "./views/Dashboard";
import LoginApp from "./components/login/login";
import Errors from "./views/Errors";
import Viewapplication from "./views/Viewapplication";
import Addapplication from "./views/Addapplication";
import viewtool from "./views/Viewtool";
import Addtool from "./views/Addtool";
import Viewproject from "./views/Viewproject";
import Addproject from "./views/Addproject";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path: "/login",
    layout: LoginLayout,
    component: LoginApp
  },
  {
    path: "/home",
    layout: DefaultLayout,
    component: Dashboard
  },
  {
    path: "/viewapplication",
    layout: DefaultLayout,
    component: Viewapplication
  },
  {
    path: "/addapplication",
    layout: DefaultLayout,
    component: Addapplication
  },
  {
    path: "/viewtool",
    layout: DefaultLayout,
    component: viewtool
  },
  {
    path: "/addtool",
    layout: DefaultLayout,
    component: Addtool
  },
  {
    path: "/viewproject",
    layout: DefaultLayout,
    component: Viewproject
  },
  {
    path: "/addproject",
    layout: DefaultLayout,
    component: Addproject
  },
  {
    path: "/errors",
    layout: LeftNavLayout,
    component: Errors
  }
];
