var mongoose = require('mongoose');
console.log('working')
require('../models/projects.model');
var Project = mongoose.model('Project');

var project_id=0;
module.exports.addProject = async(req, res) => {  
  var project_name = req.body.project_name;
  var Project_Desc = req.body.Project_Desc;
  var visibility = req.body.visibility;
  //var status = req.body.status;
  //var project_admin = req.body.project_admin;
  var user = req.body.user;

function getproject_id()
{
    var seq=1;
    project_id=project_id+seq++; 
    var pjt_id=project_id;
    return pjt_id;
}
  Project
    .create({
      project_id: await getproject_id(),
      project_name: project_name,
      Project_Desc: Project_Desc,
      visibility: visibility,
      user: user
     // status: status,
     // project_admin: project_admin

    }, function (err, project) {
      if (err) {
        res
          .status(400)
          .json(err);
          project_id=project_id-1;
      }

      else {
        res
          .status(201)
          .json(project);
          console.log('Successfully posted');
           console.log(project);

      }
    });
};

module.exports.getAllProject = (req, res) => {
  Project.find(function (err, project) {
    if (err) {
      console.log(err);
    }
    else {
      res.json(project);
    }
  });
};

module.exports.projectsGetOne = (req, res) => {
  var projectsId = req.params.projectsId;
  Project
    .findById(projectsId)
    .exec(function (err, doc) {
      var response = {
        status: 200,
        message: doc
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        response.status = 404;
        response.message = {
          "message": "ProjectId not found " + id
        };
      }
      res
        .status(response.status)
        .json(response.message);
    });

};

module.exports.projectsUpdateOne = (req, res) => {
  var projectsId = req.params.projectsId;
  console.log(projectsId);
  Project
    .findById(projectsId)
    .exec(function (err, project) {
      if (err) {
        res
          .status(500)
          .json(err);
        return;
      } else if (!project) {
        res
          .status(404)
          .lson({
            "message": "projectId not found " + projectsId
          });
       
        return;
      }

      project.project_name = req.body.project_name;
      project.Project_Desc = req.body.Project_Desc;
      project.visibility = req.body.visibility;
      project.user = req.body.user;
      //project.project_admin = req.body.project_admin;
      project.updated_on = new Date();

      project
        .save(function (err, projectUpdated) {
          if (err) {
            res
              .status(500)
              .json(err);
          } else {
            res
              .status(204)
              .json();
          }
        });
    });
};

module.exports.projectDeleteOne = (req, res) => {
  var projectsId = req.params.projectsId;
  Project
    .findByIdAndRemove(projectsId)
    .exec(function (err, project) {
      if (err) {
        res
          .status(404)
          .json(err);
      } else {
        res
          .status(204)
          .json();
      }
    })
};

