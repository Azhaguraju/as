var mongoose = require('mongoose');
require('../models/applications.model');
var Application = mongoose.model('Application');

var application_id=0;
module.exports.addApplication = async(req, res) => {
  var application_name = req.body.application_name;
  var application_version = req.body.application_version;
  var api_technology = req.body.api_technology;
  var techstack = req.body.techstack;
  var environment = req.body.environment;
  var developed_by = req.body.developed_by;
  var implemented_by = req.body.implemented_by;
  var visibility = req.body.visibility;
  
  function getapplication_id()
{
    var seq=1;
    application_id=application_id+seq++; 
    var app_id=application_id;
    return app_id;
}
 
  Application
    .create({
      application_id: await getapplication_id(),
      application_name: application_name,
      application_version: application_version,
      api_technology: api_technology,
      techstack: techstack,
      environment: environment,
      developed_by: developed_by,
      implemented_by: implemented_by,
      visibility: visibility
      
    }, function (err, application) {
      if (err) {
        res
          .status(400)
          .json(err);
          application_id=application_id-1;
          console.log(err)
      }

      else {
        res
          .status(201)
          .json(application);
          console.log(application)          
      }
    });
};

module.exports.getAllApplication = (req, res) =>  {
  Application.find(function (err, application) {
    if (err) {
      res.status(404)
        .json({ message: err });
    }
    else {
      res.status(200)
        .json(application);
    }
  });
};

module.exports.applicationsGetOne =  (req, res) => {
  var applicationsId = req.params.applicationsId;
  Application
    .findById(applicationsId)
    .exec(function (err, doc) {
      var response = {
        status: 200,
        message: doc
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        response.status = 404;
        response.message = {
          "message": "Application Id not found " + id
        };
      }
      res
        .status(response.status)
        .json(response.message);
    });

};

module.exports.applicationsUpdateOne = (req, res) => {
  var applicationsId = req.params.applicationsId;
  Application
    .findById(applicationsId)
    .exec(function (err, application) {
      if (err) {
        res
          .status(500)
          .json(err);
        return;
      } else if (!application) {
        res
          .status(404)
          .lson({
            "message": "Application Id not found " + applicationsId
          });
        return;
      }
      application.application_name = req.body.application_name;
      application.application_version = req.body.application_version;
      application.uiapitechnology = req.body.uiapitechnology;
      application.techstack = req.body.techstack;
      application.environment = req.body.environment;
      application.implementedby = req.body.implementedby;
      application.visibility = req.body.visibility;
      application.updated_on = new Date();

      application
        .save(function (err, applicationUpdated) {
          if (err) {
            res
              .status(500)
              .json(err);
          } else {
            res
              .status(204)
              .json(applicationUpdated);
          }
        });
    });
};

module.exports.applicationsDeleteOne =  (req, res) => {
  var applicationsId = req.params.applicationsId;
  Application
    .findByIdAndRemove(applicationsId)
    .exec(function (err, application) {
      if (err) {
        res
          .status(404)
          .json(err);
      } else {
        res
          .status(204)
          .json();
      }
    })
};

