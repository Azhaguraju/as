var mongoose = require('mongoose');
require('../models/projects.model');
var Project = mongoose.model('Project');

var user_id=0;
var adduser = async(req, res, project) => {
  var user_name = req.body.user_name;
  var role = req.body.role;

  function getuser_id()
  {
      var seq=1;
      user_id=user_id+seq++; 
      var ur_id=user_id;
      return ur_id;
  }

  project.users.push({
    user_id: await getuser_id(),
    user_name: user_name,
    role: role
  });
  project.save(function (err, projectUpdated) {
    if (err) {
      res
        .status(500)
        .json(err);
        user_id=user_id-1;
    } else {
      res
        .status(200)
        .json(projectUpdated);
    }
  });
};

module.exports.usersAddOne = (req, res) => {
  var id = req.params.projectsId;
  Project
    .findById(id)
    .select('users')
    .exec(function (err, doc) {
      var response = {
        status: 200,
        message: doc
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        response.status = 404;
        response.message = {
          "message": "ProjectId not found " + id
        };
      }
      if (doc) {
        adduser(req, res, doc);
      } else {
        res
          .status(response.status)
          .json(response.message);
      }
    });
};

module.exports.userDeleteOne = (req, res) => {
  var projectsId = req.params.projectsId;
  var usersId = req.params.usersId;
  Project
    .findById(projectsId)
    .select('users')
    .exec(function (err, project) {
      var thisUser;
      var response = {
        status: 200,
        message: {}
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!project) {
        response.status = 404;
        response.message = {
          "message": "Project ID not found " + id
        };
      } else {
        thisUser = project.users.id(usersId);
        if (!thisUser) {
          response.status = 404;
          response.message = {
            "message": "UsersId not found " + usersId
          };
        }
      }
      if (response.status !== 200) {
        res
          .status(response.status)
          .json(response.message);
      } else {
        project.users.id(usersId).remove();
        project.save(function (err, projectUpdated) {
          if (err) {
            res
              .status(500)
              .json(err);
          } else {
            res
              .status(204)
              .json({ message: "Success" });
          }
        });
      }
    });
};

module.exports.usersUpdateOne = (req, res) => {
  var projectsId = req.params.projectsId;
  var usersId = req.params.usersId;
  Project
    .findById(projectsId)
    .select('users')
    .exec(function (err, project) {
      var thisUser;
      var response = {
        status: 200,
        message: {}
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!project) {
        response.status = 404;
        response.message = {
          "message": "Project ID not found " + id
        };
      } else {
        thisUser = project.users.id(usersId);
        if (!thisUser) {
          response.status = 404;
          response.message = {
            "message": "usersId not found " + usersId
          };
        }
      }
      if (response.status !== 200) {
        res
          .status(response.status)
          .json(response.message);
      } else {
        thisUser.user_name = req.body.user_name;
        thisUser.role = req.body.role;
        project.save(function (err, projectUpdated) {
          if (err) {
            res
              .status(500)
              .json(err);
          } else {
            res
              .status(204)
              .json();
          }
        });
      }
    });

};

module.exports.getAllUser = (req, res) => {
  var id = req.params.projectsId;
  Project
    .findById(id)
    .select('users')
    .exec(function (err, doc) {
      var response = {
        status: 200,
        message: doc
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        response.status = 404;
        response.message = {
          "message": "ProjectId not found " + id
        };
      }
      if (doc) {
        res.json(doc);
      } else {
        res
          .status(response.status)
          .json(response.message);
      }
    });
};


module.exports.usersGetOne = (req, res) => {
  var projectsId = req.params.projectsId;
  var usersId = req.params.usersId;
  Project
    .findById(projectsId)
    .select('users')
    .exec(function (err, project) {
      var thisUser;
      var response = {
        status: 200,
        message: {}
      };
      if (err) {
        response.status = 500;
        response.message = err;
      } else if (!project) {
        response.status = 404;
        response.message = {
          "message": "Project ID not found " + id
        };
      } else {
        thisUser = project.users.id(usersId);
        if (!thisUser) {
          response.status = 404;
          response.message = {
            "message": "usersId not found " + usersId
          };
        }
      }
      if (response.status !== 200) {
        res
          .status(response.status)
          .json(response.message);
      }
      if (project) {
        res.json(thisUser);
      }
    });
};