var mongoose = require('mongoose');
require('../models/tools.model');
var Tool = mongoose.model('Tool');

var tool_id=0;
module.exports.addTool = async(req, res) => {
  var tool_name = req.body.tool_name;
  var tool_version = req.body.tool_version;
  var technology = req.body.technology;
  var window_objects = req.body.window_objects;
  var screen_objects = req.body.screen_objects;
  var field_objects = req.body.field_objects;
  var visibility = req.body.visibility;

  function gettool_id()
{
    var seq=1;
    tool_id=tool_id+seq++; 
    var tl_id=tool_id;
    return tl_id;
}
 
Tool
    .create({
      tool_id: await gettool_id(),
      tool_name: tool_name,
      tool_version: tool_version,
      technology: technology,
      window_objects: window_objects,
      screen_objects: screen_objects,
      field_objects: field_objects,
      visibility: visibility

    }, function (err, tool) {
      if (err) {
        res
          .status(400)
          .json(err);
          tool_id=tool_id-1;
          console.log(err)
      }

      else {
        res
          .status(201)
          .json(tool);
          console.log("Success")
          console.log(tool)
          
      }
    });
};

module.exports.getAllTool = (req, res) => {
  Tool.find({}, function (err, data) {
    if (err) {
      console.log(err);
    }
    else {
      res.json(data);
    }
  });
};

module.exports.toolGetOne = (req, res) => {
  var toolId = req.params.toolId;
  console.log('GET toolId', toolId);
  Tool
    .findById(toolId)
    .exec(function (err, doc) {
      var response = {
        status: 200,
        message: doc
      };
      if (err) {
        console.log("Error finding tool");
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        console.log("ToolId not found in database", id);
        response.status = 404;
        response.message = {
          "message": "ToolId not found " + id
        };
      }
      res
        .status(response.status)
        .json(response.message);
    });

};

module.exports.toolsUpdateOne = (req, res) => {
  console.log("inside toolupdate function");
  var toolId = req.params.toolId;
  console.log('GET applicationsId', toolId);
  Tool
    .findById(toolId)
    .exec(function (err, tool) {
      if (err) {
        console.log("Error finding tool");
        res
          .status(500)
          .json(err);
        return;
      } else if (!tool) {
        console.log("ToolId not found in database", toolId);
        res
          .status(404)
          .lson({
            "message": "ToolId not found " + toolId
          });
        return;
      }
      tool.tool_name = req.body.tool_name;
      tool.tool_version = req.body.tool_version;
      tool.technology = req.body.technology;
      tool.windowobjects = req.body.windowobjects;
      tool.screenobjects = req.body.screenobjects;
      tool.fieldobjects = req.body.fieldobjects;
      tool.visibility = req.body.visibility;
      tool.updated_on = new Date();

      tool
        .save(function (err, toolUpdated) {
          if (err) {
            res
              .status(500)
              .json(err);
          } else {
            res
              .status(204)
              .json();
          }
        });
    });
};

module.exports.toolDeleteOne = (req, res) => {
  console.log("Inside tool Delete");
  var toolId = req.params.toolId;
  Tool
    .findByIdAndRemove(toolId)
    .exec(function (err, tool) {
      if (err) {
        res
          .status(404)
          .json(err);
      } else {
        console.log("Tool deleted, id:", toolId);
        res
          .status(204)
          .json();
      }
    })
};
