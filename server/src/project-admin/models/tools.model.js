const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let toolSchema = new Schema({
  tool_id: {
    type: String,
    unique: [true, 'Tool Id should be unique'],
    required: [true, 'Tool Id required']
  },
  tool_name: {
    type: String,
    unique: [true, 'Tool Name should be unique'],
    required: [true, 'Tool Name required'],
    minlength: 4,
    maxlength: 20
  },
  tool_version: {
    type: String,
    required: [true, 'Tool Version required'],
    minlength: 2,
    maxlength: 20
  },
  technology: {
    type: String,
    required: [true, 'Technology required'],
    minlength: 4,
    maxlength: 20
  },
  window_objects: {
    type: String,
    required: [true, 'Windowobjects required'],
    minlength: 4,
    maxlength: 20
  },
  screen_objects: {
    type: String,
    required: [true, 'Screenobjects required'],
    minlength: 4,
    maxlength: 20
  },
  field_objects: {
    type: String,
    required: [true, 'Fieldobjects required'],
    minlength: 4,
    maxlength: 20
  },
  visibility: {
    type: String,
    required: [true, 'visibility required'],
    minlength: 4,
    maxlength: 20
  },
  status: {
    type: String,
    required: [true, 'Status required'],
    default: 'active',
    minlength: 4,
    maxlength: 20
  },
  created_by: {
    type: String,
    default:'admin',
    minlength: 4,
    maxlength: 20
  },
  created_on: {
    type: Date,
    "default": Date
  },
  updated_by: {
    type: String,
    default:'admin',
    minlength: 4,
    maxlength: 20
  },
  updated_on: {
    type: Date,
    "default": Date
  },
}, {
    collection: 'tools'
  });

module.exports = mongoose.model('Tool', toolSchema);