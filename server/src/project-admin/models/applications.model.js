const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let applicationSchema = new Schema({
  application_id: {
    type: String,
    unique: [true, 'Application Id should be unique'],
    required: [true, 'Application Id required'],
  },
  application_name: {
    type: String,
    unique: [true, 'Application Name should be unique'],
    required: [true, 'Application Name required'],
    minlength: 4,
    maxlength: 20
  },
  application_version: {
    type: String,
    required: [true, 'Application version required'],
    minlength: 2,
    maxlength: 20
  },
  api_technology: {
    type: String,
    required: [true, 'UI/API technology required'],
    minlength: 4,
    maxlength: 20
  },
  techstack: {
    type: String,
    required: [true, 'Tech Stack required'],
    minlength: 4,
    maxlength: 20
  },
  environment: {
    type: String,
    required: [true, 'Environment required'],
    minlength: 4,
    maxlength: 20
  },
  developed_by: {
    type: String,
    required: [true, 'developed_by required'],
    minlength: 4,
    maxlength: 20
  },
  implemented_by: {
    type: String,
    required: [true, 'implemented_by required'],
    minlength: 4,
    maxlength: 20
  },
  visibility: {
    type: String,
    required: [true, 'visibility required'],
    minlength: 4,
    maxlength: 20
  },
  status: {
    type: String,
    required: [true, 'Status required'],
    default: 'active',
    minlength: 4,
    maxlength: 20
  },
  created_by: {
    type: String,
   default: 'admin',
    minlength: 4,
    maxlength: 20
  },
  created_on: {
    type: Date,
    "default": Date
  },
  updated_by: {
    type: String,
    default: 'admin',
    minlength: 4,
    maxlength: 20
  },
  updated_on: {
    type: Date,
    "default": Date
  },
}, {
    collection: 'applications'
  });

module.exports = mongoose.model('Application', applicationSchema);
