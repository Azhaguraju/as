const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/*let userSchema = new Schema({

    user_id: {
        type: String,
        unique: [true, "User Id should be unique"],
        required: [true, "User Id required"]
    },
    user_name: {
        type: String,
        required: [true, "User Name required"],
        minlength: 4,
        maxlength: 20
    },
    role: {
        type: String,
        required: [true, "Role required"],
        minlength: 4,
        maxlength: 20
    },
});*/

let projectSchema = new Schema({
    project_id: {
        type: String,
        unique: [true, "Project Id should be unique"],
        required: [true, "Project Id required"]
    },
    project_name: {
        type: String,
        unique: [true, "Project Name should be unique"],
        //required: [true, "Project Name required"],
        minlength: 4,
        maxlength: 20
    },
    Project_Desc: {
        type: String,
        //required: [true, "Description required"],
        minlength: 4,
        maxlength: 20
    },
    visibility: {
        type: String,
        default: 'private',
        //required: [true, "Visibility required"],
        minlength: 4,
        maxlength: 20
    },
    status: {
        type: String,
        default: 'active',
       // required: [true, "Status required"],
        minlength: 4,
        maxlength: 20
    },
    project_admin: {
        type: String,
        default: 'Azhagu',
      //  required: [true, "Project Admin required"],
        minlength: 4,
        maxlength: 20
    },
    created_by: {
        type: String,
        default: 'admin',
        // required: [true, "created_by required required"],
        minlength: 4,
        maxlength: 20
    },
    created_on: {
        type: Date,
        "default": Date
    },
    updated_by: {
        type: String,
        default: 'admin',
       // required: [true, "updated_by required required"],
        minlength: 4,
        maxlength: 20
    },
    updated_on: {
        type: Date,
        "default": Date
    },
    //users: [userSchema]
   /* user_name: {
        type: String,
        // required: [true, "User Name required"],
        minlength: 4,
        maxlength: 20
    },
    role: {
        type: String,
       // required: [true, "Role required"],
        minlength: 4,
        maxlength: 20
    },*/
    user: {
        type: Array,
        required: [true, "User required"]
    }
});

module.exports = mongoose.model("Project", projectSchema);
