var express = require('express');
var router = express.Router();

var applicationController = require('../controllers/application.controller');
var toolController = require('../controllers/tool.controller');
var projectController = require('../controllers/project.controller');
var userController = require('../controllers/user.controller');

// Application routes
router
  .route('/applications')
  .post(applicationController.addApplication)
  .get(applicationController.getAllApplication)

router
  .route('/applications/:applicationsId')
  .get(applicationController.applicationsGetOne)
  .put(applicationController.applicationsUpdateOne)
  .delete(applicationController.applicationsDeleteOne);

// Tool routes
router
  .route('/tools')
  .post(toolController.addTool)
  .get(toolController.getAllTool)

router
  .route('/tools/:toolId')
  .get(toolController.toolGetOne)
  .put(toolController.toolsUpdateOne)
  .delete(toolController.toolDeleteOne);

// Project routes
router
  .route('/projects')
  .post(projectController.addProject)
  .get(projectController.getAllProject)

router
  .route('/projects/:projectsId')
  .get(projectController.projectsGetOne)
  .put(projectController.projectsUpdateOne)
  .delete(projectController.projectDeleteOne)

// User routes
router
  .route('/projects/:projectsId/users')
  .post(userController.usersAddOne)
  .get(userController.getAllUser)

router
  .route('/projects/:projectsId/users/:usersId')
  .get(userController.usersGetOne)
  .put(userController.usersUpdateOne)
  .delete(userController.userDeleteOne);

module.exports = router;