const express = require('express')
const bodyParser= require('body-parser')
cors = require('cors')
const app= express()
var routes = require('./src/project-admin/routes/index.route');
const mongoose= require('mongoose')

mongoose.connect('mongodb://192.168.22.39:27017/AutoQ-BA',{ useNewUrlParser: true })
mongoose.Promise=global.Promise;
const db=mongoose.connection
db.on('error',(error)=>console.error(error))
db.once('open',()=> console.log('Connected to Database'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use('/api', routes);
//const TestManagementRouter = require('./test-module/routes/TestMangement')
//app.use('/TestManagement',TestManagementRouter)
app.listen(4000,() => console.log('Server Started'))
